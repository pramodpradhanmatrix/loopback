#!/bin/bash

# Start the PostgreSQL service
sudo service postgresql start

# Switch to the postgres user and run psql
sudo su postgres <<EOF

# Run psql with the specified commands
psql <<SQL
CREATE USER loopback WITH PASSWORD 'test_123';
ALTER USER loopback CREATEDB;
CREATE DATABASE citydb WITH OWNER loopback;
GRANT ALL PRIVILEGES ON DATABASE citydb TO loopback;
\c citydb;
CREATE TABLE city (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);
SQL

# Exit from the PostgreSQL user
exit

EOF
