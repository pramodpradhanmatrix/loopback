# Use Ubuntu 20 as the base image
FROM ubuntu:20.04

# Set the working directory
WORKDIR /workspace/loopback3/loopback3-demo
COPY ./ /workspace/loopback3/loopback3-demo

# Install necessary packages
RUN apt-get update -y && \
    apt-get install -y \
    sudo \
    wget \
    curl \
    lsb-release \
    gnupg \
    git \
    vim
# Install NVM
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

# Activate NVM in the current shell session
SHELL ["/bin/bash", "--login", "-c"]

# Manually initialize NVM and install Node.js 16
#RUN source ~/.nvm/nvm.sh && \
   # nvm install 16 && \
   # nvm use 16 && \
   # npm install -g npm@7.24.0

# Continue with the rest of your Dockerfile...

# Setup PostgreSQL 14 without interactive prompts
RUN echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list && \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    apt-get update -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y postgresql-14

# Create DB and table in PostgreSQL using a script
COPY loopback3-demo/init-postgres.sh /docker-entrypoint-initdb.d/
RUN chmod +x /docker-entrypoint-initdb.d/init-postgres.sh

# Switch back to the root user
USER root

# Initialize the database cluster
RUN rm -rf /var/lib/postgresql/14/main && \
    sudo -u postgres /usr/lib/postgresql/14/bin/initdb -D /var/lib/postgresql/14/main --auth-local trust --auth-host scram-sha-256 --no-instructions

# Copy the application files
COPY . .

# Install dependencies
# RUN npm install

# Expose the port your app runs on
EXPOSE 3000

# Start PostgreSQL service and your Node.js application
CMD service postgresql start && npm start

