# Use Ubuntu 20 as the base image
FROM ubuntu:20.04

# Set the working directory
WORKDIR /workspace/loopback3/loopback3-demo
COPY ./ /workspace/loopback3/loopback3-demo


# Install necessary packages
RUN apt-get update -y && \
    apt-get install -y \
    sudo \
    wget \
    curl \
    lsb-release \
    gnupg

# Setup PostgreSQL 14 without interactive prompts
RUN echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list && \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    apt-get update -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y postgresql-14

# Initialize the database cluster
RUN /etc/init.d/postgresql start && \
    rm -rf /var/lib/postgresql/14/main && \
     sudo -u postgres /usr/lib/postgresql/14/bin/initdb -D /var/lib/postgresql/14/main --auth-local trust --auth-host scram-sha-256 --no-instructions
    #sudo -u postgres /usr/lib/postgresql/14/bin/initdb -D /var/lib/postgresql/14/main --auth-local peer --auth-host scram-sha-256 --no-instructions
# Install Node.js
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs

# Copy the application files
COPY . .

# Install dependencies
RUN npm install

# Expose the port your app runs on
EXPOSE 3000

# Start PostgreSQL service and your Node.js application
CMD service postgresql start && npm start

